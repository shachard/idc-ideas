let escape = require('sanitize-html');

function Message(id, message, fromId, fromName, toId, toName, sendTime){
    this.id = id;
    this.message = escape(message);
    this.fromId = fromId;
    this.fromName = escape(fromName);
    this.toId = toId;
    this.toName = escape(toName);
    this.sendTime = sendTime;
}

module.exports = Message;