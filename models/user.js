let escape = require('sanitize-html');

function UserCredentials(id, username, profileId, name){
    this.id = id;
    this.username = username;
    this.profileId = profileId;
    this.name = escape(name);
}

module.exports = UserCredentials;