let escape = require('sanitize-html');
let method = Problem.prototype;

function Problem(id, description, authorId, authorName, status, chosenIdea, postTime, is_mine){
    this.id = id;
    this.description = escape(description);
    this.authorId = authorId;
    this.authorName = escape(authorName);
    this.status = status;
    this.ideas = [];
    this.chosenIdea = chosenIdea;
    this.sendTime = postTime;
    this.is_mine = is_mine;

    if(this.chosenIdea == undefined){
        this.chosenIdea = -1;
    }
}

method.setIdeas = function(ideas){
    this.ideas = ideas;
};

module.exports = Problem;