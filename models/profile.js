let escape = require('sanitize-html');

function Profile(id, name, badges, problems, ideas, profileStatistics){
    this.id = id;
    this.name = escape(name);
    this.badges = badges;
    this.problems = problems;
    this.ideas = ideas;
    this.profileStatistics = profileStatistics;
}

module.exports = Profile;