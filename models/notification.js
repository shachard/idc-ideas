let escape = require('sanitize-html');

function Notification(id, text, postTime, link){
    this.id = id;
    this.text = escape(text);
    this.postTime = postTime;
    this.link = link;
}



module.exports = Notification;