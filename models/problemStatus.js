let ProblemStatus = {"OPEN": "OPEN",
                "SOLVED": "SOLVED"};
Object.freeze(ProblemStatus);

module.exports = ProblemStatus;