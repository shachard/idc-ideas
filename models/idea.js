let path = require('path');
let Privacy = require(path.join(__dirname,'privacy.js'));
let escape = require('sanitize-html');

function Idea(id, authorId, authorName, idea, privacy, postTime, is_mine){
    this.id = id;
    this.authorId = authorId;
    this.authorName = escape(authorName);
    this.idea = escape(idea);
    if(privacy){
        this.privacy = privacy;
    }
    else {
        this.privacy = Privacy.PRIVATE;
    }
    this.postTime = postTime;
    this.is_mine = is_mine;
}



module.exports = Idea;