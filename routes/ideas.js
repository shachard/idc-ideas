let express = require('express');
let path = require('path');
let router = express.Router();
let DAL = require(path.join(__dirname,'../logic/DAL.js'));
let Privacy = require(path.join(__dirname,'../models/privacy.js'));
let dal = new DAL();


router.get('/', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    res.json(dal.getAllIdeas(user.id));
});

router.get('/profile/:id', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let profileId = req.params.id;
    let is_mine = profileId == user.id;
    let privacy_level = is_mine ? Privacy.PRIVATE : Privacy.PUBLIC;

    res.json(dal.getIdeas(profileId, privacy_level, is_mine));
});

router.post("/add", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let newIdea = JSON.parse(req.body);
    if(newIdea.privacy < 0 || newIdea.privacy > 1){
        res.status(500).render();
        return;
    }
  let newId = dal.addPersonalIdea(newIdea.idea, user.id, newIdea.privacy);
  res.send(newId.toString());
});

router.delete("/delete/:id", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let idToDelete = req.params.id;
    res.send(dal.deleteIdea(idToDelete, user.id).toString());
});

router.post("/edit/:id", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let ideaToEdit = JSON.parse(req.body);
    let idToUpdate = req.params.id;
    if(ideaToEdit.privacy < 0 || ideaToEdit.privacy > 1){
        res.status(500).render();
        return;
    }
  res.send(dal.updateIdea(idToUpdate, ideaToEdit.idea, user.id, ideaToEdit.privacy).toString());
});

module.exports = router;
