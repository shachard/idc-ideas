let express = require('express');
let path = require('path');
let router = express.Router();
let DAL = require(path.join(__dirname,'../logic/DAL.js'));
let dal = new DAL();

router.get('/', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    res.json(dal.getUnreadNotifications(user.id));
});

router.get('/:id/read', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let notification = dal.getNotificationById(req.params.id, user.id);
    if (notification == null){
        res.status(500).render();
    }

    dal.setNotificationAsRead(notification.id, user.id);
    res.writeHead(302, {'Location': notification.link});
    res.end();
});

module.exports = router;
