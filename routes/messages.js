let express = require('express');
let path = require('path');
let router = express.Router();
let DAL = require(path.join(__dirname,'../logic/DAL.js'));
let dal = new DAL();

router.get('/get', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    res.json(dal.getAllMessages(user.id));
});

router.post("/add", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let newMessage = JSON.parse(req.body);
    let newId = dal.sendMessage(user.id, Number(newMessage.to), newMessage.message);

    // Add new message notification
    dal.addNotification(newMessage.to, 'You received a new private message from ' + user.name, '/messages');
    res.send(newId.toString());
});

module.exports = router;
