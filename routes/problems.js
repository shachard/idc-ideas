let express = require('express');
let path = require('path');
let router = express.Router();
let DAL = require(path.join(__dirname,'../logic/DAL.js'));
let Privacy = require(path.join(__dirname,'../models/privacy.js'));
let dal = new DAL();

/* GET home page. */
router.get('/', function(req, res, next) {
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    res.json(dal.getAllProblems(user.id));
});

router.post("/add", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let newProblem = JSON.parse(req.body);
    let newId = dal.addProblem(newProblem.description, user.id);
    res.send(newId.toString());
});

router.delete("/delete/:id", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let idToDelete = req.params.id;
    res.send(dal.deleteProblem(idToDelete, user.id).toString());
});

router.post("/edit/:id", function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let problemToEdit = JSON.parse(req.body);
    let idToUpdate = req.params.id;
    res.send(dal.updateProblem(idToUpdate, problemToEdit.description, user.id).toString());
});

router.get('/profile/:id', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let profileId = req.params.id;
    let is_mine = profileId == user.id;
    res.json(dal.getProfileProblems(profileId, is_mine));
});

router.get('/:id/ideas', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let problemId = req.params.id;
    let ideas = dal.getProblemIdeas(problemId, user.id);
    res.json(ideas);
});

router.post('/:id/ideas/add', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }
    let problemId = req.params.id;
    let problemInfo = dal.getProblemById(problemId, user.id);
    if(problemInfo == null){
        res.status(500).render();
        return;
    }

    let newIdea = JSON.parse(req.body);
    let newId = dal.addProblemIdea(problemInfo.id, newIdea.idea, user.id);

    // Send a notification to the problem author:
    if(!problemInfo.is_mine){
        dal.addNotification(problemInfo.authorId, user.name + ' has suggested a new idea for your problem', '/problem/' + problemInfo.id);
    }
    res.send(newId.toString());
});


router.get('/:problem_id/ideas/choose/:idea_id', function(req, res){
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - NO ACCESS
        res.status(403).render();
        return;
    }

    let problemId = req.params.problem_id;
    let ideaId = req.params.idea_id;

    let problemInfo = dal.getProblemById(problemId, user.id);
    if(problemInfo == null || !problemInfo.is_mine){
        res.status(500).render();
        return;
    }

    let ideaInfo = dal.getIdeaById(ideaId, user.id);
    if(ideaInfo == null){
        res.status(500).render();
        return;
    }
    let result = dal.chooseAnswer(problemId,user.id, ideaId);
    // Send a notification to the problem author:
    if(!ideaInfo.is_mine){
        dal.addNotification(ideaInfo.authorId, user.name + ' chose your idea as the solution for his problem', '/problem/' + problemInfo.id);
    }

    res.send(result.toString());
});

module.exports = router;
