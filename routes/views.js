let express = require('express');
let path = require('path');
let router = express.Router();
let DAL = require(path.join(__dirname,'../logic/DAL.js'));
let privacy = require(path.join(__dirname,'../models/privacy.js'));
let dal = new DAL();


/* GET home page. */
router.get('/', function(req, res, next) {
  if(!req.session.user){
      // If the user is not logged in - redirect to login page
      res.writeHead(302, {'Location': '/users/login'});
      res.end();
      return;
  }
  res.render('ideas.html');
});

router.get('/profile/:id', function(req, res) {
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - redirect to login page
        res.writeHead(302, {'Location': '/users/login'});
        res.end();
        return;
    }

    let profileId = req.params.id;
    let is_mine = profileId == user.id;
    let profile_name = dal.getProfileInfo(profileId);
    if(profile_name == null){
        res.status(500).render();
        return;
    }

    res.render('profile.html', {profile_name: profile_name, is_mine: is_mine});
});

router.get('/profile', function(req, res) {
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - redirect to login page
        res.writeHead(302, {'Location': '/users/login'});
        res.end();
        return;
    }

    res.writeHead(302, {'Location': '/profile/' + user.id});
    res.end();
});

router.get('/problem/:id', function(req, res) {
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - redirect to login page
        res.writeHead(302, {'Location': '/users/login'});
        res.end();
        return;
    }

    let problemId = req.params.id;
    let problemInfo = dal.getProblemById(problemId, user.id);
    if(problemInfo == null){
        res.status(500).render();
        return;
    }

    res.render('problem.html', {problem: problemInfo});
});


router.get('/problemChamber', function(req, res) {
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - redirect to login page
        res.writeHead(302, {'Location': '/users/login'});
        res.end();
        return;
    }
    res.render('problemChamber.html');
});

router.get('/messages', function(req, res) {
    let user = req.session.user;
    if(!user){
        // If the user is not logged in - redirect to login page
        res.writeHead(302, {'Location': '/users/login'});
        res.end();
        return;
    }

    res.render('messages.html', {profileId: user.id});
});


module.exports = router;
