let sqlite = require('sqlite-sync');
let path = require('path');

let UserCredentials = require(path.join(__dirname,'../models/user.js'));
let Message = require(path.join(__dirname,'../models/message.js'));
let Notification = require(path.join(__dirname,'../models/notification.js'));
let Problem = require(path.join(__dirname,'../models/problem.js'));
let Idea = require(path.join(__dirname,'../models/idea.js'));
let IdeaType = require(path.join(__dirname,'../models/ideaType.js'));
let Privacy = require(path.join(__dirname,'../models/privacy.js'));
let ProblemStatus = require(path.join(__dirname,'../models/problemStatus.js'));
let NotificationStatus = require(path.join(__dirname,'../models/notificationStatus.js'));

let method = DAL.prototype;

function DAL() {
    this.db = sqlite.connect('ideas.db');
}

// ######################  USERS #######################

method.login = function(user, pass){
    let result = this.db.run(`SELECT users.id, users.username, profiles.id as profile_id, profiles.name 
                              FROM users 
                              LEFT JOIN profiles ON users.id = profiles.userid 
                              WHERE username = ? AND pass = ?`, [user, pass]);
    if(result.length == 0){
        return null;
    }
    let row = result[0];
    return new UserCredentials(row.id, row.username, row.profile_id, row.name);
};

method.isUsernameExists = function(user){
    let result = this.db.run('SELECT id FROM users WHERE username = ?', [user]);
    return result.length >= 1;
};

method.register = function(name, user, pass){
    let userId = this.db.insert('users', {username: user, pass: pass});
    if(!userId){
        return null;
    }

    let profileId = this.db.insert('profiles', {name: name, userid: userId});
    if(!profileId){
        return null;
    }
    return new UserCredentials(userId, user, profileId, name);
};

// ######################  PROFILES #######################
method.getProfileInfo = function(profileId){
    let result = this.db.run(`SELECT name 
                              FROM profiles 
                              WHERE userid = ?`, [profileId]);
    if(result.length == 0){
        return null;
    }
    return result[0].name;
};


// ######################  IDEAS #######################

method.getIdeas = function(profileId, privacy_level, is_mine){
    let queryResult = this.db.run(`SELECT ideas.id, ideas.author, profiles.name, ideas.idea, ideas.privacy, ideas.insertion_time 
                                   FROM ideas 
                                   LEFT JOIN profiles ON profiles.userid = ideas.author 
                                   WHERE author = ? AND type = ? AND privacy >= ? 
                                   ORDER BY insertion_time desc`,
                                    [profileId, IdeaType.PERSONAL, privacy_level]);
    let result = [];
    for(let i in queryResult){
        let row = queryResult[i];
        let idea = new Idea(row.id,
                            row.author,
                            row.name,
                            row.idea,
                            row.privacy,
                            row.insertion_time,
                            is_mine);
        result.push(idea);
    }
    return result;
};

method.getIdeaById = function(ideaId, userId){
    let queryResult = this.db.run(`SELECT ideas.id, ideas.author, profiles.name, ideas.idea, ideas.privacy, ideas.insertion_time 
                                   FROM ideas 
                                   LEFT JOIN profiles ON profiles.userid = ideas.author 
                                   WHERE ideas.id = ? 
                                   ORDER BY insertion_time desc`,
        [ideaId]);
    if(queryResult.length == 0){
        return null;
    }
    let row = queryResult[0];
    return new Idea(row.id,
            row.author,
            row.name,
            row.idea,
            row.privacy,
            row.insertion_time,
            userId == row.author);
};

method.getAllIdeas = function(profileId){
    let queryResult = this.db.run(`SELECT ideas.id, ideas.author, profiles.name, ideas.idea, ideas.privacy, ideas.insertion_time 
                                   FROM ideas 
                                   LEFT JOIN profiles ON profiles.userid = ideas.author 
                                   WHERE type = ? AND privacy >= ? 
                                   ORDER BY insertion_time desc`,
        [IdeaType.PERSONAL, Privacy.PUBLIC]);
    let result = [];
    for(let i in queryResult){
        let row = queryResult[i];
        let idea = new Idea(row.id,
            row.author,
            row.name,
            row.idea,
            row.privacy,
            row.insertion_time,
            row.author == profileId);
        result.push(idea);
    }
    return result;
};


method.getProblemIdeas = function(problemId, userId){
    let queryResult = this.db.run(`SELECT ideas.id, ideas.author, profiles.name, ideas.idea, ideas.privacy, ideas.insertion_time 
                                   FROM ideas 
                                   LEFT JOIN profiles ON profiles.userid = ideas.author 
                                   WHERE target = ? and type = ?
                                   ORDER BY insertion_time asc`,
                                   [problemId, IdeaType.PROBLEM_ANSWER]);
    let result = [];
    for(let i in queryResult){
        let row = queryResult[i];
        let idea = new Idea(row.id,
            row.author,
            row.name,
            row.idea,
            row.privacy,
            row.insertion_time,
            row.author == userId);
        result.push(idea);
    }
    return result;
};

method.addPersonalIdea = function(idea, profileId, privacy){
    return this.db.insert('ideas', {author: profileId, idea: idea, privacy: privacy, type: IdeaType.PERSONAL});
};

method.addProblemIdea = function(problemId ,idea, profileId){
    return this.db.insert('ideas', {author: profileId, idea: idea, privacy: Privacy.PUBLIC, type: IdeaType.PROBLEM_ANSWER, target: problemId});
};

method.deleteIdea = function(id, profileId){
    let result = this.db.run('DELETE FROM ideas WHERE id = ? and author = ?', [id, profileId]);
    return 1;
};

method.setIdeaPrivacy = function(id, privacyLevel, profileId){
    let result = this.db.update('ideas', {privacy: privacyLevel}, {id: id, author: profileId});
    return 1;
};

method.updateIdea = function(id, text, profileId, privacyLevel){
    let result = this.db.update('ideas', {idea: text, privacy: privacyLevel}, {id: id, author: profileId});
    return 1;
};

// ######################  NOTIFICATIONS #######################

method.getUnreadNotifications = function(profileId){
    let queryResult = this.db.run(`SELECT id, text, insertion_time, link  
                                   FROM notifications 
                                   WHERE profile_id = ? and status = ?
                                   ORDER BY insertion_time desc`,
        [profileId, NotificationStatus.UNREAD]);
    let result = [];
    for(let i in queryResult){
        let row = queryResult[i];
        let notification = new Notification(row.id,
            row.text,
            row.insertion_time,
            row.link);
        result.push(notification);
    }
    return result;
};

method.getNotificationById = function(notificationId, profileId){
    let queryResult = this.db.run(`SELECT id, text, insertion_time, link  
                                   FROM notifications 
                                   WHERE profile_id = ? and id = ?
                                   ORDER BY insertion_time desc`,
        [profileId, notificationId]);
    if(queryResult.length == 0){
        return null;
    }
    let row = queryResult[0];
    return new Notification(row.id,
                row.text,
                row.insertion_time,
                row.link);
};

method.addNotification = function(profileId, text, link){
    return this.db.insert('notifications', {profile_id: profileId, text: text, status: NotificationStatus.UNREAD, link: link});
};

method.setNotificationAsRead =  function(id, profileId){
    let result = this.db.update('notifications', {status: NotificationStatus.READ}, {id: id, profile_id: profileId});
    return 1;
};

// ######################  PROBLEMS #######################

method.getAllProblems = function(profileId){
    let queryResult = this.db.run(`SELECT problems.id, problems.author, profiles.name , problems.description, problems.status, problems.chosen_idea, problems.insertion_time
                                   FROM problems
                                   LEFT JOIN profiles ON profiles.userid = problems.author
                                   ORDER BY insertion_time desc`);
    let result = [];
    for(let i in queryResult){
        let row = queryResult[i];
        let problem = new Problem(row.id,
                                       row.description,
                                       row.author,
                                       row.name,
                                       row.status,
                                       row.chosen_idea,
                                       row.insertion_time,
                                       row.author == profileId);
        result.push(problem);
    }
    return result;
};

method.getProfileProblems = function(profileId, is_mine){
    let queryResult = this.db.run(`SELECT problems.id, problems.author, profiles.name , problems.description, problems.status, problems.chosen_idea, problems.insertion_time
                                   FROM problems
                                   LEFT JOIN profiles ON profiles.userid = problems.author
                                   WHERE problems.author = ?
                                   ORDER BY insertion_time desc`, [profileId]);
    let result = [];
    for(let i in queryResult){
        let row = queryResult[i];
        let problem = new Problem(row.id,
            row.description,
            row.author,
            row.name,
            row.status,
            row.chosen_idea,
            row.insertion_time,
            is_mine);
        result.push(problem);
    }
    return result;
};

method.getProblemById = function(problemId, userId){
    let queryResult = this.db.run(`SELECT problems.id, problems.author, profiles.name , problems.description, problems.status, problems.chosen_idea, problems.insertion_time
                                   FROM problems
                                   LEFT JOIN profiles ON profiles.userid = problems.author
                                   WHERE problems.id = ?
                                   ORDER BY insertion_time desc`, [problemId]);
    if(queryResult.length == 0){
        return null;
    }
    let row = queryResult[0];
    return new Problem(row.id,
        row.description,
        row.author,
        row.name,
        row.status,
        row.chosen_idea,
        row.insertion_time,
        row.author == userId);
};

method.addProblem = function(description, profileId){
    return this.db.insert('problems', {author: profileId, description: description, status: ProblemStatus.OPEN});
};

method.chooseAnswer = function(id, profileId, ideaId){
    let result = this.db.update('problems', {status: ProblemStatus.SOLVED, chosen_idea: ideaId}, {id: id, author: profileId});
    return 1;
};

method.setProblemStatus = function(id, profileId, status){
    let result = this.db.update('problems', {status: status}, {id: id, author: profileId});
    return 1;
};

method.updateProblem = function(id, text, profileId){
    let result = this.db.update('problems', {description: text}, {id: id, author: profileId});
    return 1;
};

method.deleteProblem = function(id, profileId){
    let result = this.db.run('DELETE FROM problems WHERE id = ? and author = ?', [id, profileId]);
    return 1;
};

// ######################  MESSAGES #######################
method.getAllMessages = function(profileId){
    let queryResult = this.db.run(`SELECT messages.id, message, fromId, fromProfile.name as fromName, toId, toProfile.name as toName, insertion_time 
                                   FROM messages
                                   LEFT JOIN profiles fromProfile ON fromProfile.userid = messages.fromId
                                   LEFT JOIN profiles toProfile ON toProfile.userid = messages.toId 
                                   WHERE toId = ? OR fromId = ?
                                   ORDER BY insertion_time desc`, [profileId, profileId]);
    let result = [];
    for(let i in queryResult){
        let message = new Message(queryResult[i].id,
                                  queryResult[i].message,
                                  queryResult[i].fromId,
                                  queryResult[i].fromName,
                                  queryResult[i].toId,
                                  queryResult[i].toName,
                                  queryResult[i].insertion_time);
        result.push(message);
    }
    return result;
};

method.sendMessage = function(from, to, message){
    return this.db.insert('messages', {message: message, fromId: from, toId: to});
};

module.exports = DAL;