let timeout;
// $(function(){
//     load();
// });

// function load() {
//     getAllIdeas();
//     commonGetAllProblems();
//
//     getProfilePage();
//     getAllProblemsIdeas();
// }

// ####################### Main page #######################

function getAllIdeas() {
    fetch('/ideas', {credentials: "same-origin"})
        .then(function (response) {
            return response.json();
        })
        .then(function (ideas) {
            clearIdeasTable();
            ideas.forEach(function (idea) {
                appendIdea(idea);
            });
        });
}

function commonGetAllProblems(){
    fetch('/problems', {credentials: "same-origin"})
        .then(function(response) {
            return response.json();
        })
        .then(function(problems) {
            clearProblemsTable();
            problems.forEach(function(problem){
                appendProblem(problem);
            });
        });
}

function appendIdea(idea){
	let row= $('<tr></tr>').attr('id', 'row' + idea.id);

	row.append($('<td></td>').text(idea.id).addClass('font-weight-bold'));
	row.append($('<td></td>').attr('id','ideaText' + idea.id).text(idea.idea));

    let text = '<td><a href="/profile/';
    text += idea.authorId;
    text += '">';
    text += idea.authorName;
    text += '</a></td>';
    row.append($(text));

	if (idea.is_mine) {
        // operations - only if the author id matches
        row.append($('<td></td>').append(
            $('<a></a>').addClass('btn btn-default delete').data('id', idea.id).click(deleteIdea).append(
                $('<span></span>').addClass('glyphicon glyphicon-trash').attr('aria-hidden', 'true')
            )
        ).append(
            $('<a></a>').addClass('btn btn-default edit').data('id', idea.id).click(editIdea).append(
                $('<span></span>').addClass('glyphicon glyphicon-pencil').attr('aria-hidden', 'true')
            )
        ));
    }

    $("#ideasTableBody").append(row);
}

function appendProblem(problem){
    let row= $('<tr></tr>').attr('id', 'row' + problem.id);
    row.append($('<td></td>').text(problem.id).addClass('font-weight-bold'));

    let textDescription = '<td><a href="/problem/';
    textDescription += problem.id;
    textDescription += '">';
    textDescription += problem.description;
    textDescription += '</a></td>';
    row.append($(textDescription));

    let text = '<td><a href="/profile/';
    text += problem.authorId;
    text += '">';
    text += problem.authorName;
    text += '</a></td>';
    row.append($(text));

    row.append($('<td></td>').attr('id','problemText' + problem.id).text(problem.status));

    if (problem.is_mine) {
        row.append($('<td></td>').append(
            $('<a></a>').addClass('btn btn-default delete').data('id', problem.id).click(deleteProblem).append(
                $('<span></span>').addClass('glyphicon glyphicon-trash').attr('aria-hidden', 'true')
            )
        ).append(
            $('<a></a>').addClass('btn btn-default edit').data('id', problem.id).click(editProblem).append(
                $('<span></span>').addClass('glyphicon glyphicon-pencil').attr('aria-hidden', 'true')
            )
        ));
    }

    $("#problemsTableBody").append(row);
}

function deleteIdea(){
	let ideaId = $(this).data('id');
    fetch('/ideas/delete/' + ideaId, {method: 'DELETE', credentials: "same-origin"})
        .then(function(response) {
			return response.text();
        })
		.then(function(response){
			if(response == "1"){
				$('#row' + ideaId).remove();
				alertMessage("The Idea was successfully removed!", "success")
			}
			else{
				alertMessage("An error has occurred while trying to remove Idea id=" + ideaId, 'danger');
			}
		});
}

function deleteProblem(){
    let problemId = $(this).data('id');
    fetch('/problems/delete/' + problemId, {method: 'DELETE', credentials: "same-origin"})
        .then(function(response) {
            return response.text();
        })
        .then(function(response){
            if(response == "1"){
                $('#row' + problemId).remove();
                alertMessage("The Problem was successfully removed!", "success")
            }
            else{
                alertMessage("An error has occurred while trying to remove Problem id=" + problemId, 'danger');
            }
        });
}

function submitForm(){
    let ideaId = $('#ideaId').val();
    let ideaText = $('#idea').val();
    let ideaPrivacy;
    if ($('#ideaPrivate').is(":checked")) {
        ideaPrivacy = 0;
    } else {
        ideaPrivacy = 1;
    }

    let data = {idea: ideaText, privacy: ideaPrivacy};
    if (ideaId === "") {
        // New Idea:
        fetch('/ideas/add', {method: 'POST', body: JSON.stringify(data), credentials: "same-origin"})
            .then(function (response) {
                return response.text();
            })
            .then(function (response) {
                $('#newIdeaModal').modal('hide');
                if (isNaN(response)) {
                    alertMessage("An error has occurred while trying to add a new story", 'danger');
                    return;
                }

                load();
            });
    }
	else{
		// Editing existing idea:
        fetch('/ideas/edit/' + ideaId , {method: 'POST', body: JSON.stringify(data), credentials: "same-origin"})
            .then(function(response) {
                $('#newIdeaModal').modal('hide');
                return response;
            })
            .then(function(response){
                if(response === "0"){
                    alertMessage("An error has occurred while trying to edit the story", 'danger');
                    return;
                }

                alertMessage("The Idea was successfully edited!", "success");
                load();
            });
	}

}

function submitProblemForm(){
    let problemId = $('#problemId').val();
    let problemText = $('#problem').val();
    let data = {description: problemText};
    if (problemId == ""){
        // New problem:
        fetch('/problems/add' , {method: 'POST', body: JSON.stringify(data), credentials: "same-origin"})
            .then(function(response) {
                return response.text();
            })
            .then(function(response){
                $('#newProblemModal').modal('hide');
                if(isNaN(response)){
                    alertMessage("An error has occurred while trying to add a new story", 'danger');
                    return;
                }

                load();
            });
    }
    else{
        // Editing existing problem:
        fetch('/problems/edit/' + problemId , {method: 'POST', body: JSON.stringify(data), credentials: "same-origin"})
            .then(function(response) {
                $('#newProblemModal').modal('hide');
                return response;
            })
            .then(function(response){
                if(response == "0") {
                    alertMessage("An error has occurred while trying to edit the story", 'danger');
                    return;
                }

                alertMessage("The Problem was successfully edited!", "success");
                load();
            });
    }
}

function editIdea(){
	let ideaId = $(this).data('id');
    $('#ideaId').val(ideaId);
    $('#idea').val($('#ideaText' + ideaId).text());
    $('#myModalLabel').text("Edit idea:");
    $('#ideaPublic').attr('checked', true).button("refresh");
    // $('#ideaPrivate').attr('checked', false).button("refresh");
    $('#newIdeaModal').modal('show');
}

function editProblem(){
    let problemId = $(this).data('id');
    $('#problemId').val(problemId);
    $('#problem').val($('#problemText' + problemId).text());
    $('#myModalLabelProblem').text("Edit problem:");
    $('#newProblemModal').modal('show');
}

function OnClickAddIdeaButton(){
    $('#myModalLabel').text("Add a new idea:");
    $('form')[0].reset();
    $('#ideaId').val('');
    $('#ideaPrivate').attr('checked', true).button("refresh");
    $('#newIdeaModal').modal('show');
}

function OnClickAddProblemButton(){
    $('#myModalLabelProblem').text("Add a new problem:");
    $('form')[0].reset();
    $('#problemId').val('');
    $('#newProblemModal').modal('show');
}

function clearIdeasTable(){
	$("#ideasTableBody").empty();
}

function clearProblemsTable(){
    $("#problemsTableBody").empty();
}

function alertMessage(message, classer){
	classer = 'alert alert-' + classer; 
	$('.userMessage').fadeIn(10).html(message).addClass(classer);
	clearTimeout(timeout);
	timeout = setTimeout(function(){
		$('.userMessage').fadeOut(1000,function(){
			$(this).html('').removeClass(classer);
		});
	}, 5000);
}

// ####################### Profile page #######################

function OnClickSendPrivateMessageButton(){
    $('#myModalLabelMessage').text("Send a private message:");
    $('form')[0].reset();
    $('#newMessageModal').modal('show');
}

function submitMessageForm(){
    let messageText = $('#message').val();
    let pathArray = window.location.pathname.split('/');
    let toProfileId = pathArray.pop();
    let data = {to: toProfileId, message: messageText};

    fetch('/messages/add', {method: 'POST', body: JSON.stringify(data), credentials: "same-origin"})
        .then(function (response) {
            return response.text();
        })
        .then(function (response) {
            if (isNaN(response)) {
                alertMessage("An error has occurred while trying to send a message", 'danger');
                return;
            }

            $('#newMessageModal').modal('hide');
            alertMessage("The message was successfully delivered!", "success");
        });
}

function getProfilePage(){
    getAllProfilesProblems();
    getAllProfilesIdeas();
}

function getAllProfilesIdeas() {
    let pathArray = window.location.pathname.split('/');
    let profileId = pathArray.pop();

    fetch('/ideas/profile/' + profileId, {credentials: "same-origin"})
        .then(function (response) {
            return response.json();
        })
        .then(function (ideas) {
            clearProfilesIdeasTable();
            ideas.forEach(function (idea) {
                appendProfilesIdea(idea);
            });
        });
}

function getAllProfilesProblems(){
    let pathArray = window.location.pathname.split('/');
    let profileId = pathArray.pop();

    fetch('/problems/profile/' + profileId, {credentials: "same-origin"})
        .then(function(response) {
            return response.json();
        })
        .then(function(problems) {
            clearProfilesProblemsTable();
            problems.forEach(function(problem){
                appendProfilesProblem(problem);
            });
        });
}

function clearProfilesIdeasTable(){
    $("#profilesIdeasTableBody").empty();
}

function clearProfilesProblemsTable(){
    $("#profilesProblemsTableBody").empty();
}

function appendProfilesIdea(idea){
    let row= $('<tr></tr>').attr('id', 'row' + idea.id);

    row.append($('<td></td>').text(idea.id).addClass('font-weight-bold'));
    row.append($('<td></td>').attr('id','ideaText' + idea.id).text(idea.idea));

    if (idea.is_mine) {
        row.append($('<td></td>').append(
            $('<a></a>').addClass('btn btn-default delete').data('id', idea.id).click(deleteIdea).append(
                $('<span></span>').addClass('glyphicon glyphicon-trash').attr('aria-hidden', 'true')
            )
        ).append(
            $('<a></a>').addClass('btn btn-default edit').data('id', idea.id).click(editIdea).append(
                $('<span></span>').addClass('glyphicon glyphicon-pencil').attr('aria-hidden', 'true')
            )
        ));
    } else {
        let text = '<td><a href="/profile/';
        text += idea.authorId;
        text += '">';
        text += idea.authorName;
        text += '</a></td>';
        row.append($(text));
    }

    $("#profilesIdeasTableBody").append(row);
}

function appendProfilesProblem(problem){
    let row= $('<tr></tr>').attr('id', 'row' + problem.id);
    row.append($('<td></td>').text(problem.id).addClass('font-weight-bold'));

    let text = '<td><a href="/problem/';
    text += problem.id;
    text += '">';
    text += problem.description;
    text += '</a></td>';
    row.append($(text));

    if (!problem.is_mine) {
        let text = '<td><a href="/profile/';
        text += problem.authorId;
        text += '">';
        text += problem.authorName;
        text += '</a></td>';
        row.append($(text));
    }

    row.append($('<td></td>').attr('id','problemText' + problem.id).text(problem.status));

    if (problem.is_mine) {
        row.append($('<td></td>').append(
            $('<a></a>').addClass('btn btn-default delete').data('id', problem.id).click(deleteProblem).append(
                $('<span></span>').addClass('glyphicon glyphicon-trash').attr('aria-hidden', 'true')
            )
        ).append(
            $('<a></a>').addClass('btn btn-default edit').data('id', problem.id).click(editProblem).append(
                $('<span></span>').addClass('glyphicon glyphicon-pencil').attr('aria-hidden', 'true')
            )
        ));
    }

    $("#profilesProblemsTableBody").append(row);
}

// ####################### Problem page #######################


function getAllProblemsIdeas() {
    let pathArray = window.location.pathname.split('/');
    let problemId = pathArray.pop();

    fetch(' /problems/' + problemId + '/ideas', {credentials: "same-origin"})
        .then(function (response) {
            return response.json();
        })
        .then(function (ideas) {
            clearProblemsIdeasTable();
            ideas.forEach(function (idea) {
                appendProblemsIdea(idea);
            });
        });
}

function clearProblemsIdeasTable(){
    $("#proposedIdeasTableBody").empty();
}

function appendProblemsIdea(idea){
    let row= $('<tr></tr>').attr('id', 'row' + idea.id);
    row.append($('<td></td>').text(idea.id).addClass('font-weight-bold'));
    row.append($('<td></td>').attr('id','ideaText' + idea.id).text(idea.idea));

    let text = '<td><a href="/profile/';
    text += idea.authorId;
    text += '">';
    text += idea.authorName;
    text += '</a></td>';
    row.append($(text));

    if (isMyProblem && problemOpen) {
        row.append(
            $('<a></a>').addClass('btn btn-default edit').data('id', idea.id).click(chooseIdea).append(
                $('<span></span>').addClass('glyphicon glyphicon-ok').attr('aria-hidden', 'true')
            )
        );
    }

    if (chosenSolution == idea.id){
        row.attr('style', 'border-color: green !important; border: solid;')
    }

    $("#proposedIdeasTableBody").append(row);
}

function chooseIdea() {
    let ideaId = $(this).data('id');
    let pathArray = window.location.pathname.split('/');
    let problemId = pathArray.pop();

    fetch('/problems/' + problemId +'/ideas/choose/' + ideaId, {credentials: "same-origin"})
        .then(function (response) {
            return response.text();
        })
        .then(function (response) {
            if (isNaN(response)) {
                alertMessage("An error has occurred while trying to add a new story", 'danger');
                return;
            }
            getAllProblemsIdeas();
            alertMessage("The Idea was successfully chosen!", "success");
        });
}

function OnClickProposeIdeaButton(){
    $('#proposeIdeaModalLabel').text("Propose an idea:");
    $('form')[0].reset();
    $('#ideaId').val('');
    $('#proposeIdeaModal').modal('show');
}

function submitProposeIdeaForm() {
    let pathArray = window.location.pathname.split('/');
    let problemId = pathArray.pop();
    let ideaId = $('#ideaId').val();
    let ideaText = $('#idea').val();
    let data = {idea: ideaText};
    if (ideaId === "") {
        // New Idea:
        fetch('/problems/' + problemId +'/ideas/add', {method: 'POST', body: JSON.stringify(data), credentials: "same-origin"})
            .then(function (response) {
                return response.text();
            })
            .then(function (response) {
                $('#proposeIdeaModal').modal('hide');
                if (isNaN(response)) {
                    alertMessage("An error has occurred while trying to add a new story", 'danger');
                    return;
                }
                load();
            });
    }
    else{
        // Editing existing idea:
        fetch('/ideas/edit' + ideaId , {method: 'POST', body: ideaText, credentials: "same-origin"})
            .then(function(response) {
                return response.text();
            })
            .then(function(response){
                $('#proposeIdeaModal').modal('hide');
                if(response === "0"){
                    alertMessage("An error has occurred while trying to edit the story", 'danger');
                    return;
                }
                $('#ideaText' + ideaId).text(ideaText);
                alertMessage("The Idea was successfully edited!", "success");
                load();
            });
    }
}

// ####################### Messages page #######################

function clearMessagesTable(){
    $("#messagesTableBody").empty();
}

function getAllMessages(){
    fetch('/messages/get', {credentials: "same-origin"})
        .then(function(response) {
            return response.json();
        })
        .then(function(messages) {
            clearMessagesTable();
            messages.forEach(function(message){
                appendMessage(message);
            });
        });
}

function appendMessage(message){
    let row= $('<tr></tr>').attr('id', 'row' + message.id);
    row.append($('<td></td>').text(message.id).addClass('font-weight-bold'));
    row.append($('<td></td>').attr('id','messageText' + message.id).text(message.message));

    let fromText = '<td><a href="/profile/';
    fromText += message.fromId;
    fromText += '">';
    fromText += message.fromName;
    fromText += '</a></td>';
    row.append($(fromText));

    let toText = '<td><a href="/profile/';
    toText += message.toId;
    toText += '">';
    toText += message.toName;
    toText += '</a></td>';
    row.append($(toText));
    
    row.append($('<td></td>').attr('id','messageText' + message.id).text(message.sendTime));

    $("#messagesTableBody").append(row);
}

// ####################### Notifications #######################

function getAllNotifications() {
    fetch('/notifications', {credentials: "same-origin"})
        .then(function(response) {
            return response.json();
        })
        .then(function(notifications) {
            clearNotifications();
            notifications.forEach(function(notification){
                appendNotification(notification);
            });
            document.getElementById("notifications-header").innerHTML = "Notification (<b>" + notifications.length + "</b>)";
        });
}

function clearNotifications(){
    $("#notification-panel").empty();
}

function appendNotification(notification) {
    let listItem = $('<li></li>').attr('id', 'notification' + notification.id);
    listItem.append($('<a></a>').attr('href', '/notifications/' + notification.id + '/read')
        .append($('<p></p>').text(notification.text)));

    $('#notification-panel').append(listItem);
}

setInterval(function(){ getAllNotifications(); }, 60000);
